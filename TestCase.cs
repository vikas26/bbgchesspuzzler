#load "ChessPlayer.cs";
using System;
using System.Diagnostics;

public class SingleTestCase {
    public int hSize { set; get; }
    public int vSize { set; get; }
    public string[] boardToFillWith { set; get; }
    public bool printLayout { set; get; }
    public int possibleLayouts { set; get; }
}

private List<SingleTestCase> allTestCases = new List<SingleTestCase>();

public void InitTestCases () {
    allTestCases.Add (
        new SingleTestCase {
            hSize = 3,
            vSize = 3,
            boardToFillWith = new string[] {"K", "K", "R"},
            printLayout = false,
            possibleLayouts = 4
        }
    );

    allTestCases.Add (
        new SingleTestCase {
            hSize = 4,
            vSize = 4,
            boardToFillWith = new string[] {"N", "N", "N", "N", "R", "R"},
            printLayout = false,
            possibleLayouts = 8
        }
    );

    allTestCases.Add (
        new SingleTestCase {
            hSize = 3,
            vSize = 3,
            boardToFillWith = new string[] {"Q", "B"},
            printLayout = false,
            possibleLayouts = 16
        }
    );
    
    allTestCases.Add (
        new SingleTestCase {
            hSize = 7,
            vSize = 7,
            boardToFillWith = new string[] {"Q", "Q", "K", "K", "B", "B", "N"},
            printLayout = false,
            possibleLayouts = 3063828
        }
    );
}

public void Init () {
    
    // initialize test cases
    InitTestCases ();

    for (int i=0; i<allTestCases.Count; i++) {
        // start stopwatch
        Stopwatch stopWatch = new Stopwatch ();
        stopWatch.Start ();

        ChessPlayer chessPlayer = new ChessPlayer ();
        int possibleLayouts = chessPlayer.ChessBoardConfiguration (allTestCases[i].hSize, allTestCases[i].vSize, allTestCases[i].boardToFillWith, allTestCases[i].printLayout);
        
        // stop stopwatch and print elapsed time
        stopWatch.Stop ();
        TimeSpan ts = stopWatch.Elapsed;
        string timeTaken = " Time taken : " + ts.Milliseconds + " ms.";
        string testStatus = " Test case (" + (i+1).ToString () + ") ";
        string outcomes = "";
        outcomes += " Expected layouts : " + allTestCases[i].possibleLayouts + ".";
        outcomes += " Calculated layouts : " + possibleLayouts + ". ";

        if (possibleLayouts == allTestCases[i].possibleLayouts) {
            testStatus += "successful.";
        } else {
            testStatus += "unsuccessful.";
        }
        Console.WriteLine (testStatus + outcomes + timeTaken);
    }
}

Init ();
