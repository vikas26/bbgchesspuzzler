#load "ChessBoard.cs";
using System;

public class ChessPlayer {

    // creates object and finds solution of chess puzzler
    // returns possible layouts
    public int ChessBoardConfiguration (int hSize, int vSize, string[] boardToFillWith, bool printLayout) {
        // solve chess puzzler
        ChessBoard board = new ChessBoard ();
        return board.Solve (hSize, vSize, boardToFillWith, printLayout);
    }
}
