using System;

public class ChessBoard {

    // fill squares with
    private int emptyFill = 0;
    private int playerMoveFill = 1;
    private int playerFill = 2;

    // filled values and free squares
    private string[,] filledValues;
    private int[,] freeSquares;

    // layouts
    private int possibleLayouts = 0;
    private bool printLayoutsFlag;

    // moves remaining list
    private List<string> remainingPieces = new List<string>();
    private List<int> remainingCounts = new List<int>();

    // finds number of distinct layouts
    public int Solve (int m, int n, string[] boardToFillWith, bool printFlag = false) {

        filledValues = new string[m, n];
        freeSquares = new int[m, n];
        printLayoutsFlag = printFlag;

        // sorts pieces in order of filling squares
        SortPiecesByRelevance (boardToFillWith);

        // fill squares recursively
        FillRecursively (remainingPieces, remainingCounts, freeSquares, filledValues);

        return possibleLayouts;
    }

    // sorts pieces in order of filling squares
    private void SortPiecesByRelevance (string[] boardToFillWith) {
        string lastPieceName = "";
        foreach (string pieceName in boardToFillWith) {
            if (lastPieceName == pieceName) {
                remainingCounts [remainingCounts.Count - 1] += 1;
            } else {
                lastPieceName = pieceName;
                remainingPieces.Add (pieceName);
                remainingCounts.Add (1);
            }
        }
    }

    // fills recursively in free squares
    private bool FillRecursively (List<string> tempRemainingPieces, List<int> tempRemainingCounts, int[,] tempFreeSquares, string[,] tempFilledValues) {

        // No pieces left. Found solution
        if (tempRemainingPieces.Count < 1) {
            FoundPossibleLayout (tempFilledValues);
            return true;
        }
        
        // Empty free squares
        List<int> allFreeSquares = new List<int>();
        for (int i=0; i<tempFreeSquares.GetLength(0); i++) {
            for (int j=0; j<tempFreeSquares.GetLength(1); j++) {
                if (tempFreeSquares[i, j] == 0){
                    allFreeSquares.Add ((i * tempFreeSquares.GetLength(0)) + j);
                }
            }
        }

        // remaining pieces still need to fill
        int remainingPiecesCount = 0;
        for (int i=0; i<tempRemainingCounts.Count; i++) {
            remainingPiecesCount += tempRemainingCounts[i];
        }

        // free squares are less than remaining counts then no solution
        if (allFreeSquares.Count < remainingPiecesCount) {
            return false;
        }

        // all possible combinations of filling one type of peices
        List<List<int>> possibleCombinations = PossibleGroupCombinations (allFreeSquares, tempRemainingCounts[0]);
        
        // try filling one type of pieces
        foreach (List<int> oneCombination in possibleCombinations) {
            
            // deep copy of vars
            List<string> newTempRemainingPieces = new List<string>(tempRemainingPieces);
            List<int> newTempRemainingCounts = new List<int>(tempRemainingCounts);
            int[,] newTempFreeSquares = (int[,])tempFreeSquares.Clone();
            string[,] newTempFilledValues = (string[,])tempFilledValues.Clone();

            // remove first as it going to fill
            newTempRemainingPieces.RemoveAt (0);
            newTempRemainingCounts.RemoveAt (0);

            // check validity of individual combination value
            foreach (int oneValue in oneCombination) {
                int j = oneValue % tempFreeSquares.GetLength(0);
                int i = (oneValue - j) / tempFreeSquares.GetLength(0);
                bool validityFlag = true;

                if (newTempFreeSquares[i, j] != emptyFill) {
                    // already filled position
                    break;
                } else if (oneCombination[oneCombination.Count - 1] == oneValue) {
                    // last value of combination
                    // fill value and update free squares
                    newTempFilledValues[i, j] = tempRemainingPieces[0];
                    var a = UpdateFreeSquares (newTempFreeSquares, i, j, tempRemainingPieces[0]);
                    newTempFreeSquares = a.Item1;
                    validityFlag = a.Item2;
                    if (!validityFlag) { break; }

                    // fill next piece
                    FillRecursively (newTempRemainingPieces, newTempRemainingCounts, newTempFreeSquares, newTempFilledValues);
                } else {
                    // free square
                    // fill value and update free squares
                    newTempFilledValues[i, j] = tempRemainingPieces[0];
                    var b = UpdateFreeSquares (newTempFreeSquares, i, j, tempRemainingPieces[0]);
                    newTempFreeSquares = b.Item1;
                    validityFlag = b.Item2;
                    if (!validityFlag) { break; }
                }
            }
        }
        return true;
    }

    // Found possible layout
    private void FoundPossibleLayout (string[,] tempFilledValues) {
        possibleLayouts += 1;
        if (printLayoutsFlag) {
            PrintLayout (tempFilledValues);
        }
    }

    // all possible combinations 
    private List<List<int>> PossibleGroupCombinations (List<int> allFreeSquares, int groupOf) {
        List<List<int>> result = new List<List<int>>();

        int[] array = new int[groupOf];
        // creates a new array with indices as values
        for (int i = 0; i < groupOf; i++) {
            array[i] = i;
        }

        while (true) {
            // find each combination without repetition
            List<int> oneResult = new List<int>();
            for (int i = 0; i < groupOf; ++i){
                oneResult.Add (allFreeSquares[array[i]]);
            }
            result.Add (oneResult);

            // Find the last element in the array that can be incremented
            int index = groupOf - 1;
            int lastPosition = allFreeSquares.Count - 1;
            while(index >= 0 && array[index] == lastPosition) {
                lastPosition -= 1;
                index -= 1;
            }
            if (index < 0) {
                // reached lower limit
                break;
            } else {
                // increment
                array[index]++;
                // reset the ones to the right
                for(int i = index + 1; i < groupOf; ++i) {
                    array[i] = array[i-1]+1;
                }
            }
        }
        return result;
    }

    // print layout
    private void PrintLayout (string[,] tempFilledValues) {
        string layout = " === Solution number " + possibleLayouts.ToString() + " === ";
        Console.WriteLine (layout);
        for (int i=0; i<tempFilledValues.GetLength(0); i++) {
            layout = "";
            for (int j=0; j<tempFilledValues.GetLength(1); j++) {
                if (string.Equals (tempFilledValues[i, j], null)) {
                    layout += "_";
                } else {
                    layout += tempFilledValues[i, j];
                }
            }
            Console.WriteLine (layout);
        }
    }

    // updates free squares
    private Tuple<int[,], bool> UpdateFreeSquares (int[,] tempFreeSquares, int x, int y, string pieceName) {
        bool validityFlag = true;
        var a = new Tuple<int[,], bool>(tempFreeSquares, false);
        switch (pieceName) {
            case "K":
                // King - one step in any direction
                a = FillKing (tempFreeSquares, x, y);
                tempFreeSquares = a.Item1;
                validityFlag = a.Item2;
                break;
            case "Q":
                // Queen - any number of steps in hor, ver and dia
                a = FillRook (tempFreeSquares, x, y, true);
                tempFreeSquares = a.Item1;
                validityFlag = a.Item2;
                if (!validityFlag) {
                    return new Tuple<int[,], bool>(tempFreeSquares, validityFlag);
                }
                a = FillBishop (tempFreeSquares, x, y);
                tempFreeSquares = a.Item1;
                validityFlag = a.Item2;
                break;
            case "N":
                // Knight - two and half steps
                a = FillKnight (tempFreeSquares, x, y);
                tempFreeSquares = a.Item1;
                validityFlag = a.Item2;
                break;
            case "B":
                // Bishop - any number of steps in dia
                a = FillBishop (tempFreeSquares, x, y);
                tempFreeSquares = a.Item1;
                validityFlag = a.Item2;
                break;
            case "R":
                // Rook - any number of steps in hor and ver
                a = FillRook (tempFreeSquares, x, y);
                tempFreeSquares = a.Item1;
                validityFlag = a.Item2;
                break;
        }
        return new Tuple<int[,], bool>(tempFreeSquares, validityFlag);
    }

    // fill free squares for king move
    private Tuple<int[,], bool> FillKing (int[,] tempFreeSquares, int x, int y) {
        for (int i=-1; i<2; i++) {
            for (int j=-1; j<2; j++) {
                int i1 = x + i;
                int j1 = y + j;
                if (i1 >= 0 && i1 < tempFreeSquares.GetLength(0) && j1 >= 0 && j1 < tempFreeSquares.GetLength(1)) {
                    if (!CheckValidityOfSquare (tempFreeSquares, i1, j1)) {
                        return new Tuple<int[,], bool>(tempFreeSquares, false);
                    }
                    tempFreeSquares[i1, j1] = playerMoveFill;
                }
            }
        }
        tempFreeSquares[x, y] = playerFill;
        return new Tuple<int[,], bool>(tempFreeSquares, true);
    }

    // fill free squares for bishop move
    private Tuple<int[,], bool> FillBishop (int[,] tempFreeSquares, int x, int y) {

        int minLength = tempFreeSquares.GetLength(0) < tempFreeSquares.GetLength(1) ? tempFreeSquares.GetLength(0) : tempFreeSquares.GetLength(1);

        for (int i=0; i<minLength; i++) {
            // diagonal left to right and top to bottom
            int i1 = i;
            int j1 = i1 - x + y;
            if (i1 >= 0 && i1 < tempFreeSquares.GetLength(0) && j1 >= 0 && j1 < tempFreeSquares.GetLength(1)) {
                if (!CheckValidityOfSquare (tempFreeSquares, i1, j1)) {
                    return new Tuple<int[,], bool>(tempFreeSquares, false);
                }
                tempFreeSquares[i1, j1] = playerMoveFill;
            }

            // diagonal right to left and top to bottom
            int i2 = minLength - i - 1;
            int j2 = x - i2 + y;
            if (i2 >= 0 && i2 < tempFreeSquares.GetLength(0) && j2 >= 0 && j2 < tempFreeSquares.GetLength(1)) {
                if (!CheckValidityOfSquare (tempFreeSquares, i2, j2)) {
                    return new Tuple<int[,], bool>(tempFreeSquares, false);
                }
                tempFreeSquares[i2, j2] = playerMoveFill;
            }
        }

        tempFreeSquares[x, y] = playerFill;
        return new Tuple<int[,], bool>(tempFreeSquares, true);
    }

    // fill free squares for rook move
    private Tuple<int[,], bool> FillRook (int[,] tempFreeSquares, int x, int y, bool queenMove = false) {

        int maxLength = tempFreeSquares.GetLength(0) < tempFreeSquares.GetLength(1) ? tempFreeSquares.GetLength(1) : tempFreeSquares.GetLength(0);

        for (int i=0; i<maxLength; i++) {
            // top to bottom
            int i1 = i;
            int j1 = y;
            if (i1 >= 0 && i1 < tempFreeSquares.GetLength(0) && j1 >= 0 && j1 < tempFreeSquares.GetLength(1)) {
                if (!CheckValidityOfSquare (tempFreeSquares, i1, j1)) {
                    return new Tuple<int[,], bool>(tempFreeSquares, false);
                }
                tempFreeSquares[i1, j1] = playerMoveFill;
            }

            // left to right
            int i2 = x;
            int j2 = i;
            if (i2 >= 0 && i2 < tempFreeSquares.GetLength(0) && j2 >= 0 && j2 < tempFreeSquares.GetLength(1)) {
                if (!CheckValidityOfSquare (tempFreeSquares, i2, j2)) {
                    return new Tuple<int[,], bool>(tempFreeSquares, false);
                }
                tempFreeSquares[i2, j2] = playerMoveFill;
            }
        }

        if (!queenMove) {
            tempFreeSquares[x, y] = playerFill;
        }
        return new Tuple<int[,], bool>(tempFreeSquares, true);
    }

    // fill free squares for knight move
    private Tuple<int[,], bool> FillKnight (int[,] tempFreeSquares, int x, int y) {

        int[] rookMoves = {-2, -1, 1, 2};
        int finalSum = 3;
        for (int i=0; i<rookMoves.Length; i++) {
            int x1 = rookMoves[i];  // -2, -1, 1, 2
            int y1 = finalSum - Math.Abs(x1);   // 1, 2, 2, 1

            int i1 = x + x1;
            int j1 = y + y1;
            if (i1 >= 0 && i1 < tempFreeSquares.GetLength(0) && j1 >= 0 && j1 < tempFreeSquares.GetLength(1)) {
                if (!CheckValidityOfSquare (tempFreeSquares, i1, j1)) {
                    return new Tuple<int[,], bool>(tempFreeSquares, false);
                }
                tempFreeSquares[i1, j1] = playerMoveFill;
            }

            int i2 = x + x1; // -2, -1, 1, 2
            int j2 = y - y1;   // -1, -2, -2, -1
            if (i2 >= 0 && i2 < tempFreeSquares.GetLength(0) && j2 >= 0 && j2 < tempFreeSquares.GetLength(1)) {
                if (!CheckValidityOfSquare (tempFreeSquares, i2, j2)) {
                    return new Tuple<int[,], bool>(tempFreeSquares, false);
                }
                tempFreeSquares[i2, j2] = playerMoveFill;
            }
        }
        
        tempFreeSquares[x, y] = playerFill;
        return new Tuple<int[,], bool>(tempFreeSquares, true);
    }

    // is this move valid to fill free squares
    private bool CheckValidityOfSquare (int[,] tempFreeSquares, int x, int y) {
        if (tempFreeSquares[x, y] == playerFill) {
            return false;
        }
        return true;
    }
}
