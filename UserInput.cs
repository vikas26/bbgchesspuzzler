#load "ChessPlayer.cs";
using System;
using System.Diagnostics;

int hSize, vSize;
int kCount, qCount, bCount, rCount, nCount; 
List<string> boardToFillWith = new List<string>();
bool printLayout = false;

Console.Write ("Input horizontal board size : ");
hSize = Int32.Parse(Console.ReadLine());
Console.Write ("Input vertical board size : ");
vSize = Int32.Parse(Console.ReadLine());

Console.Write ("Input number of Kings : ");
kCount = Int32.Parse(Console.ReadLine());
Console.Write ("Input number of Queens : ");
qCount = Int32.Parse(Console.ReadLine());
Console.Write ("Input number of Bishops : ");
bCount = Int32.Parse(Console.ReadLine());
Console.Write ("Input number of Rooks : ");
rCount = Int32.Parse(Console.ReadLine());
Console.Write ("Input number of Knights : ");
nCount = Int32.Parse(Console.ReadLine());

boardToFillWith.AddRange (System.Linq.Enumerable.Repeat ("Q", qCount));
boardToFillWith.AddRange (System.Linq.Enumerable.Repeat ("K", kCount));
boardToFillWith.AddRange (System.Linq.Enumerable.Repeat ("R", rCount));
boardToFillWith.AddRange (System.Linq.Enumerable.Repeat ("B", bCount));
boardToFillWith.AddRange (System.Linq.Enumerable.Repeat ("N", nCount));

public void Init () {
    // start stopwatch
    Stopwatch stopWatch = new Stopwatch ();
    stopWatch.Start ();

    ChessPlayer chessPlayer = new ChessPlayer ();
    int possibleLayouts = chessPlayer.ChessBoardConfiguration (hSize, vSize, boardToFillWith.ToArray(), printLayout);
    
    // stop stopwatch and print elapsed time
    stopWatch.Stop ();
    TimeSpan ts = stopWatch.Elapsed;
    string timeTaken = " Time taken : " + ts.Milliseconds + " ms.";
    string outcomes = " Calculated layouts : " + possibleLayouts + ".";
    Console.WriteLine (outcomes + timeTaken);
}

Init ();
