# Chess Puzzler #

### Problem ###

The problem is to find all unique configurations of a set of normal chess pieces on a chess board with dimensions M×N where none of the pieces is in a position to take any of the others. Assuming the color of the piece does not matter, and that there are no pawns among the pieces.

### How do I get set up? ###

This is the way i followed while writing the code. You just need to run C# scripts. You can use IDE too but here is a way of running it like a script.

1. Install [**Sublime Text**](https://www.sublimetext.com/)
2. Install [**scriptcs**](https://packagecontrol.io/packages/scriptcs)
3. Download repo.

### How to run test cases? ###

1. Open command line, go to directory where repo is present.
2. Execute (in command line) ``` scriptcs TestCase.cs ```

### How to execute with user specified inputs? ###

1. Open command line, go to directory where repo is present.
2. Execute (in command line) ``` scriptcs UserInput.cs ``` 
3. Input integer values when asked.

### References ###

1. [Chess puzzler guide by Ivan Voroshilin's Blog](http://ivoroshilin.com/2015/02/05/toughest-backtracking-problems-in-algorithmic-competitions/)

### ###